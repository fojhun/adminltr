import { RestClientService } from './../../core/service/rest-client.service';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthParam } from '../model/auth-param';
import { LocalStorageService } from 'src/app/core/service/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  token: string;
  userInfo: {
    company: string;
    period: string;
    token: string;
    userName: string;
  };

  constructor(
    private storage: LocalStorageService,
    private httpService: RestClientService,
    private router: Router
  ) {
    const user = JSON.parse(this.storage.read('user'));

    if (user) {
      this.setUser(user);
      this.setToken(user.token);
    }
  }

  setToken(token: string) {
    this.token = token;
  }
  setUser(user) {
    this.userInfo = user;
  }
  login(body: AuthParam) {
    let url = 'Authentication';
    const responseType = 'text';
    return this.httpService.post<any>(url, body, responseType);
  }
  refreshToken(body) {
    this.login(body);
  }
  logout() {
    localStorage.clear();

    this.router.navigate(['/auth/login']);
  }
}
