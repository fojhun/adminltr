import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/core/service/local-storage.service';
import { AuthenticationService } from '../../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private auth: AuthenticationService,
    private router: Router,
    private storage: LocalStorageService
  ) {}
  loginForm: FormGroup;
  submitted: boolean = false;
  fieldTextType: boolean;
  ngOnInit(): void {
    this.initForm();
  }
  initForm(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  saveForm() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.auth.login(this.loginForm.value).subscribe((res) => {
        if (res) {
          const response = JSON.parse(res);
          this.storage.save('user', res);
          this.auth.setToken(response.token);
          this.auth.setUser(response);

          this.router.navigate(['/']);
        }
      });
    }
  }

  resetForm() {
    this.loginForm.reset();
  }
}
