import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./page/login/login.component";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
];
@NgModule({
  declarations: [LoginComponent],
  imports: [RouterModule.forChild(routes), CommonModule, ReactiveFormsModule],
})
export class AuthModule {}
