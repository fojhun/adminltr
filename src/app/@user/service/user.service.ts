import { User, UserById } from './../model/user';
import { ResultObject } from './../model/result-object';

import { RestClientService } from './../../core/service/rest-client.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import {  HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'src/app/@auth/service/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private httpService: RestClientService,
    private http: HttpClient,
    public auth: AuthenticationService
  ) {}
  getUser(skip: number, top: number, count = true) {
    let url = `odata/CustomerSuppliers`;
    url += `?$skip=${skip}`;
    url += `&$top=${top}`;
    url += `&$count=${count}`;

    return this.httpService.get<ResultObject<User[]>>(url).pipe(map((res) => res));
  }
  getUserById(id:number) {
    let url = `odata/CustomerSuppliers/${id}`;

    return this.httpService.get<UserById>(url).pipe(map((res) => res));
  }
  toBase64(str) {
    return window.btoa(encodeURIComponent(str));
  }

  fromBase64(str) {
    return decodeURIComponent(window.atob(str));
  }
}
