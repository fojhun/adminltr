import { Col } from './../../../@shared/component/table/model/col';
import { User, UserById } from './../../model/user';

import { Component, OnInit } from '@angular/core';

import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  users: User[];
  column: Col[];
  infoColumn: Col[][];
  totalRecords: number;
  rows: number;
  skip: number = 0;
  isLoading: boolean;
  isInfoLoading: boolean;
  top: number = 10;
  customerInfo: User[];
  display: boolean = false;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUserList();
    this.createColumn();
    this.getInfoColumns();
  }

  getUserList() {
    this.isLoading = true;
    this.users = null;
    this.userService.getUser(this.skip, this.top, true).subscribe((res) => {
      this.isLoading = false;
      this.users = res.value;
      this.totalRecords = res['@odata.count'];
    });
  }
  paginate(event) {
    this.skip = event.page;
    this.top = event.rows;
    this.getUserList();
  }
  createColumn() {
    const that = this;
    this.column = [
      { field: 'CargoCompany', header: 'company', type: 'text' },
      { field: 'Code', header: 'code', type: 'text' },
      { field: 'ContractStatus', header: 'contract-status', type: 'boolean' },
      {
        field: 'CustomerSupplierApprovalStatus',
        header: 'approval-status',
        type: 'text',
      },
      {
        field: 'CustomerSupplierType',
        header: 'supplier-type',
        type: 'text',
      },
      { field: 'IsActive', header: 'active', type: 'boolean' },
      { field: 'Name', header: 'name', type: 'text' },
      {
        field: 'PersonelGovermentNumber',
        header: 'goverment-number',
        type: 'numeric',
      },
      { field: 'Oid', header: 'id', type: 'numeric' },
      { field: 'PhoneNumber', header: 'phone-number', type: 'text' },
      {
        field: 'TaxNumberForCustomerSupplier',
        header: 'taxnumber',
        type: 'text',
      },
      { field: 'Title', header: 'title', type: 'text' },
      {
        header: 'manage',
        type: 'action',
        columnActions: [
          {
            icon: 'pi pi-info-circle text-blue  font-size-20',
            title: 'show-info',
            onClick(item) {
              that.showModal(item);
            },
          },
        ],
      },
    ];
  }

  showModal(item: User): void {

    this.display = true;
    this.getUserById(item.Oid);
  }
  getUserById(id: number) {
    this.isInfoLoading = true;
    this.customerInfo=null;
    this.userService.getUserById(id).subscribe((res: User) => {
      this.customerInfo = [
        {
          CargoCompany: res.CargoCompany,
          Code: res.Code,
          ContractStatus: res.ContractStatus,
          CustomerSupplierApprovalStatus: res.CustomerSupplierApprovalStatus,
          CustomerSupplierType: res.CustomerSupplierType,
          IsActive: res.IsActive,
          Name: res.Name,
          Oid: res.Oid,
          PersonelGovermentNumber: res.PersonelGovermentNumber,
          PhoneNumber: res.PhoneNumber,
          TaxNumberForCustomerSupplier: res.TaxNumberForCustomerSupplier,
          Title: res.Title
        }
      ];
      this.isInfoLoading = false;
    });
  }
  getInfoColumns(): void {
    this.infoColumn = [
      [
        { field: 'CargoCompany', header: 'company', type: 'text' },
        { field: 'Code', header: 'code', type: 'text' },
        { field: 'ContractStatus', header: 'contract-status', type: 'boolean' },
        {
          field: 'CustomerSupplierApprovalStatus',
          header: 'approval-status',
          type: 'text',
        },
      ],
      [

        {
          field: 'CustomerSupplierType',
          header: 'supplier-type',
          type: 'text',
        },
        { field: 'IsActive', header: 'active', type: 'boolean' },
        { field: 'Name', header: 'name', type: 'text' },
        {
          field: 'PersonelGovermentNumber',
          header: 'goverment-number',
          type: 'numeric',
        },
      ],

      [
        { field: 'Oid', header: 'id', type: 'numeric' },
        { field: 'PhoneNumber', header: 'phone-number', type: 'text' },
        {
          field: 'TaxNumberForCustomerSupplier',
          header: 'taxnumber',
          type: 'text',
        },
        { field: 'Title', header: 'title', type: 'text' },
      ],
    ];
  }
}
