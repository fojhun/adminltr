export interface User {
  CargoCompany: string;
  Code: string;
  ContractStatus: boolean;
  CustomerSupplierApprovalStatus: string;
  CustomerSupplierType: string;
  IsActive: boolean;
  Name: string;
  Oid: number;
  PersonelGovermentNumber: number;
  PhoneNumber: string;
  TaxNumberForCustomerSupplier: string;
  Title: string;
}

export interface UserById extends User {
  '@odata.context': 'http://193.53.103.178:5312/api/odata/$metadata#CustomerSuppliers/$entity';
}
