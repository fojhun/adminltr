import { Routes } from '@angular/router';

import { UserComponent } from './page/layout/user.component';

export const userRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    // children: [{ path: '', component:  }],
  },
];

export const routedComponents = [UserComponent];
