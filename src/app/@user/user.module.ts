import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routedComponents, userRoutes } from './user.routing';
import { SharedModule } from '../@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [routedComponents],
  imports: [
    RouterModule.forChild(userRoutes),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
  ],
})
export class UserModule {}
