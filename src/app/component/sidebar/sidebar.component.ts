import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { navigation } from 'src/app/core/model/navigation';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  menuList: navigation[] = [
    {
      title: 'Home ',
      path: '/',
      icon:'pi pi-home'
    },
    {
      title: 'CustomerSuppliers ',
      path: '/customer-suppliers',
      icon:'pi pi-users'
    },
  ];
  constructor() {}

  ngOnInit() {}
}
