
import { inOutAnimation } from 'src/app/@shared/animation/animation';
import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/app/@auth/service/authentication.service';

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.scss'],
  animations:[inOutAnimation]
})
export class ProfileMenuComponent implements OnInit {
  isShow: boolean = false;
  user: {
    company: string;
    period: string;
    token: string;
    userName: string;
  }={
    company: '',
    period: '',
    token: '',
    userName: ''
};
  constructor(private auth: AuthenticationService) {}

  ngOnInit(): void {
    this.user=this.auth.userInfo;

  }
  logout(){
    this.auth.logout();
  }
}
