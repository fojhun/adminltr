import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './core/guards/auth.guard';
import { LayoutComponent } from './page/layout/layout.component';

const routes: Routes = [
  {
    path: "auth",
    loadChildren: () =>
      import("src/app/@auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "",
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "customer-suppliers",
        loadChildren: () =>
          import("src/app/@user/user.module").then((m) => m.UserModule),
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
