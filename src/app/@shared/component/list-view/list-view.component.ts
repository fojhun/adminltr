import { Col } from './../table/model/col';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class ListViewComponent implements OnInit {
  @Input() data;
  @Input() column: Col[][] = [];


  constructor() {}

  ngOnInit(): void {}
}
