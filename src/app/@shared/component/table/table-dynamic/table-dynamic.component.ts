import { ColumnActions } from './../../../model/column-actions';
import { Col } from './../model/col';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-dynamic',
  templateUrl: './table-dynamic.component.html',
  styleUrls: ['./table-dynamic.component.scss'],
})
export class TableDynamicComponent implements OnInit {
  @Input() cols: Col[];
  @Input() data;
  @Input() colsAction:ColumnActions[]

  constructor() {}

  ngOnInit(): void {}

}
