export interface Col{
   field?: string,
   header: string,
   type?: "text" | "numeric" | "boolean" | "action";
   columnActions?:ColumnActions[];
}
export interface ColumnActions{
  icon:string;
  title: string;
  onClick(row);
}
