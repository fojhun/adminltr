import {
  animate,
  AUTO_STYLE,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
const DEFAULT_DURATION = 300;
export const inOutAnimation = trigger('inOutAnimation', [
  transition(':enter', [
    style({ height: 0, visibility: 'hidden', opacity: 0 }),
    animate(
      DEFAULT_DURATION + 'ms ease-out',
      style({ height: AUTO_STYLE, visibility: AUTO_STYLE, opacity: 1 })
    ),
  ]),
  transition(':leave', [
    style({ height: AUTO_STYLE, visibility: AUTO_STYLE, opacity: 1 }),
    animate(
      DEFAULT_DURATION + 'ms ease-in',
      style({ height: 0, visibility: 'hidden', opacity: 0 })
    ),
  ]),
]);


