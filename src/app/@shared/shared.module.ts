import { SppinerComponent } from './component/sppiner/sppiner.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {PaginatorModule} from 'primeng/paginator';
import { ReplaceNullWithTextPipe } from './pipe/replace-null-with-text.pipe';
import { TableDynamicComponent } from './component/table/table-dynamic/table-dynamic.component';
import {TableModule} from 'primeng/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { CardComponent } from './component/card/card.component';
import {DialogModule} from 'primeng/dialog';
import { ListViewComponent } from './component/list-view/list-view.component';
@NgModule({
  declarations: [ ReplaceNullWithTextPipe, TableDynamicComponent,SppinerComponent,CardComponent,ListViewComponent],
  imports: [RouterModule, CommonModule,TableModule,PaginatorModule,ProgressSpinnerModule,DialogModule],
  exports: [
    RouterModule,
    TableModule,
    TableDynamicComponent,
    ListViewComponent,
    ReplaceNullWithTextPipe,
    DialogModule,
    SppinerComponent,
    CardComponent,
    ProgressSpinnerModule,
    PaginatorModule
  ],

  providers: [],
})
export class SharedModule {}
