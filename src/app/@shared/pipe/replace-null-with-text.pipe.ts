import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
  name: "replaceNullWithText",
})
export class ReplaceNullWithTextPipe implements PipeTransform {
  transform(value: any, repleceText: string): any {
    if (!value ?? typeof value === "undefined" ?? value === null) {
      return repleceText;
    }

    return value;
  }
}
