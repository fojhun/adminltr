export interface ColumnSetting {
  id?: number;
  field?: string;
  title?: string;
  format?: "dropDown" | "scroll";
  route?: string;
  type?: "text" | "numeric" | "long-text" | "boolean";
  isCurrency?: boolean;
  icon?: string;
}
