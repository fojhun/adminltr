export interface ColumnActions{
    icon:string;
    title: string;
    onClick(row);
}
