import { Component } from '@angular/core';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent  {
  layoutMode = 'static';
  topbarMenuActive: boolean;

  staticMenuDesktopInactive: boolean;


  topbarItemClick: boolean;



  constructor() {}

  onMenuButtonClick(event) {


    this.topbarMenuActive = false;

    if (this.isDesktop()) {
      this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive;
    }

    event.preventDefault();
  }

  isDesktop() {
    return window.innerWidth > 1024;
  }

  isStatic() {
    return this.layoutMode === 'static';
  }
}
