import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/@auth/service/authentication.service';

import { environment } from './../../../environments/environment.prod';

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RestClientService  {
  constructor(public http: HttpClient,private router: Router,)
  {}

  get<T>(url: string): Observable<T> {

    return this.http
      .get<T>(`${this.getBaseServer()}${url}`)
      .pipe(catchError(this.handleError));
  }

  post<T>(url: string, body?: any, responseType?: string): Observable<T> {
    return this.http
      .post<T>(
        `${this.getBaseServer()}${url}`,
        body,
        this.theOptions(body, responseType)
      )
      .pipe(catchError(this.handleError));
  }

  put<T>(url: string, body?: any): Observable<T> {
    return this.http
      .put<T>(`${this.getBaseServer()}${url}`, body, this.theOptions(body))
      .pipe(catchError(this.handleError));
  }

  delete<T>(url: string, body?: any): Observable<T> {
    return this.http
      .delete<T>(`${this.getBaseServer()}${url}`, this.theOptions(body))
      .pipe(catchError(this.handleError));
  }

  patch<T>(url: string, body?: any): Observable<T> {
    return this.http
      .patch<T>(`${this.getBaseServer()}${url}`, body, this.theOptions(body))
      .pipe(catchError(this.handleError));
  }
  getBaseServer(): string {
    return environment.baseUrl;
  }
  theOptions(params?: any, responseType?: any) {
    return {
      params: params ? params : null,
      responseType: responseType ? responseType : 'text/plain',
    };
  }
  private handleError(error: HttpErrorResponse) {
    if (error.status === 401 || error.status === 403) {
      localStorage.clear();

      this.router.navigate(['/auth/login']);
    }
    return throwError(error);
  }
}
