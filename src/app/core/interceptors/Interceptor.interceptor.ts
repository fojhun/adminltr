import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from 'src/app/@auth/service/authentication.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(public auth: AuthenticationService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    if (this.auth.token) {
      req = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${this.auth.token}`),
      });
    }

    if (!req.headers.has('Content-Type')) {
      req = req.clone({
        headers: req.headers.set(
          'Content-Type',
          'application/json; charset=utf-8'
        ),
      });
    }

    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });

    return next.handle(req);
  }
}
